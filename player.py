# player.py

class Player:
    def __init__(self, name):
        self.name = name
        self.progress = 0

    def update_progress(self, level_completed):
        if level_completed:
            self.progress += 1