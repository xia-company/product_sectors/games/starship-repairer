# level.py

class Level:
    def __init__(self, level_number):
        self.level_number = level_number
        self.completed = False

    def complete(self):
        self.completed = True