```mermaid
classDiagram
    class Game:
        - int score
        - int oxygen_level
        - Player player
        - Level current_level
        - Leaderboard leaderboard
        + __init__(self)
        + start_game(self)
        + end_game(self)
    class Player:
        - str name
        - int progress
        + __init__(self, name)
        + update_progress(self, level_completed)
    class Level:
        - int level_number
        - bool completed
        + __init__(self, level_number)
        + complete(self)
    class Leaderboard:
        - list[Player] players
        + __init__(self)
        + add_player(self, player)
        + get_leaderboard(self)
```