```python
[
    ("main.py", "Main program flow"),
    ("game.py", "Game logic and state management"),
    ("player.py", "Player class and progress tracking"),
    ("level.py", "Level class and completion status"),
    ("leaderboard.py", "Leaderboard class and player management"),
]
```