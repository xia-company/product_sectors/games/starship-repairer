```mermaid
sequenceDiagram
    participant M as Main
    participant G as Game
    participant L as Level
    participant LB as Leaderboard
    participant P as Player
    
    M->>G: start game
    G->>G: initialize game
    G->>G: load leaderboard
    G->>G: load current level
    G->>G: display instructions
    G->>G: start game loop
    loop While game is running
        G->>G: handle events
        G->>G: update game state
        G->>G: render game graphics
    end
    G->>G: end game
    G->>G: update leaderboard
    G->>G: display leaderboard
    G->>G: save leaderboard
    G->>G: exit game
```